defmodule MhqSiteWeb.ContactController do
  use MhqSiteWeb, :controller

  # alias MhqSite.MHQ
  # alias MhqSite.MHQ.Contact

  def main(conn, _params) do
    render(conn, "main.html")
  end

end
