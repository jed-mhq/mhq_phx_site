defmodule MhqSiteWeb.AboutController do
  use MhqSiteWeb, :controller

  alias MhqSite.MHQ
  alias MhqSite.MHQ.About

  def main(conn, _params) do
    render(conn, "main.html")
  end

end
