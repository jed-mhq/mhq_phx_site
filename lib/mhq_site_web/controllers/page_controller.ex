defmodule MhqSiteWeb.PageController do
  use MhqSiteWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
