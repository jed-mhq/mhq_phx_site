defmodule MhqSite.Repo do
  use Ecto.Repo,
    otp_app: :mhq_site,
    adapter: Ecto.Adapters.Postgres
end
